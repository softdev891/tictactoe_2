package com.wittaya.tictactoegame;

import java.util.Scanner;

/**
 *
 * @author AdMiN
 */
public class TTTGAME {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char CurrentPlayer = 'O';
    static int row, col;
    static Scanner in = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        ShowWelcome();

        while (true) {
            ShowTable();
            ShowTurn();
            InputRowCol();
            Process();
            if (finish) {
                System.exit(0);
            }
        }

    }

    public static void ShowTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void ShowWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void ShowTurn() {
        System.out.println("Turn " + CurrentPlayer);
    }

    public static void InputRowCol() {
        System.out.println("Please Input Row and Column : ");
        row = in.nextInt();
        col = in.nextInt();
    }

    public static void Process() {
        if (SetTable()) {
            if (CheckWin()) {
                finish = true;
                ShowWin();
                return;
            }
            if(CheckDraw()){
                finish  = true;
                ShowDraw();
                return;
                    
            }
            count++;
            SwitchPlayer();
        }
    }

    private static void ShowWin() {
        ShowTable();
        System.out.println(">>>"+ CurrentPlayer + "Win<<<");
    }

    public static boolean SetTable() {
        table[row - 1][col - 1] = CurrentPlayer;
        return true;
    }

    public static void SwitchPlayer() {
        if (CurrentPlayer == 'O') {
            CurrentPlayer = 'X';

        } else {
            CurrentPlayer = 'O';
        }
    }

    public static boolean CheckWin() {
        if (CheckVer()) {
            return true;
        } else if (CheckHor()) {
            return true;
        } else if (CheckConner1()) {
            return true;
        }else if (CheckConner2()) {
            return true;
        }
        return false;
    }

    public static boolean CheckVer() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col] != CurrentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean CheckHor() {
        for (int c = 0; c < table.length; c++) {
            if (table[row][c] != CurrentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean CheckConner1() {
        if(table[1][1]!=CurrentPlayer && table[2][2]!=CurrentPlayer && table[3][3]!=CurrentPlayer){
            return false;
        }
        return true;
    }
    
    public static boolean CheckConner2() {
        if(table[1][3]!=CurrentPlayer && table[2][2]!=CurrentPlayer && table[3][1]!=CurrentPlayer){
            return false;
        }
        return true;
    }

    private static boolean CheckDraw() {
        if(count == 9){
            ShowDraw();
        }
       return true;
    }

    private static void ShowDraw() {
        ShowTable();
        System.out.println("You Both are Draw Please Try To Play Again");
    }

}
